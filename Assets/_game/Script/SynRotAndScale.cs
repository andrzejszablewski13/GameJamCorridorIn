using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class SynRotAndScale : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private bool _synRot, _synScale;
    [SerializeField] private float _synScaleFactor = 1;
    private NavMeshAgent _agent;
    private Animator _anim;

    void Start()
    {
        _agent = this.GetComponentInParent<NavMeshAgent>();
        _anim = this.GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_synRot)
        {

            this.transform.rotation = PeapleInside.Instance.FakeFloor.rotation;
        }
        if(_synScale)
        {
            this.transform.localScale = 1f / (Vector3.Distance(Camera.main.transform.position, transform.position) * _synScaleFactor) * Vector3.one;
        }
        _anim.SetBool("walk", Mathf.Abs(_agent.velocity.magnitude) > 0.2f);
    }
}
