using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAboveHead : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float _chanceForTextToUpper = 0.3f;
    [SerializeField] private Vector2 _timeRangeOfRec = new Vector2(4, 7);
    [SerializeField] private Sprite[] _texts;
    private SpriteRenderer _renderer;
    void Start()
    {
        _renderer = this.GetComponent<SpriteRenderer>();
        StartCoroutine(ShowTextAboveHead());
    }

    IEnumerator ShowTextAboveHead()
    {
        while (true)
        {
            if(Random.value<_chanceForTextToUpper)
            {
                _renderer.enabled = true;
                _renderer.sprite = _texts[Random.Range(0, _texts.Length)];
            }else
            {
                _renderer.enabled = false;
            }
            yield return new WaitForSeconds(Random.Range(_timeRangeOfRec.x, _timeRangeOfRec.y));
        }
    }
}
