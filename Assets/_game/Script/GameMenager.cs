using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public enum MainTypes
{
    Money,Peaple,Anger
}
public class GameMenager : MonoBehaviour
{
    // Start is called before the first frame update
    private static GameMenager _instance;

    public static GameMenager Instance { get => _instance;}
    public delegate void moneyChange(float value);
    public delegate void peapleChange(float value);
    public delegate void angerChange(float value);
    public static moneyChange OnMoneyChange;
    public static peapleChange OnPeapleChange;
    public static angerChange OnAngerChange;
    public float Anger { get => _anger;set{  _anger =Mathf.Clamp( value,0,  300); OnAngerChange?.Invoke(_anger); } }
    public float Money { get => _money; set { _money = Mathf.Clamp(value, 0, 300); OnMoneyChange?.Invoke(_money); } }
    public float Peaple { get => _peaple; set { _peaple = Mathf.Clamp(value, 0, 300); OnPeapleChange?.Invoke(_peaple); } }

   [SerializeField] private float _anger=5, _money=10, _peaple=3;
    private void OnDisable()
    {
        OnMoneyChange = null;
        OnPeapleChange = null;
        OnAngerChange = null;
    }
    void Awake()
    {
        _instance = this;
    }
    private void Start()
    {
        Anger = Anger;
        Money = Money;
        Peaple = Peaple;
        SoundManager.Instance.PlayMusic("backgroundMusic");
    }
    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
