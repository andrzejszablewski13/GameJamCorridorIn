using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AiMoving : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform _3DFloor;
    private NavMeshAgent _agent;
    [SerializeField] private Vector2 _timeRangeOfRec=new Vector2(5,10);
    void Start()
    {
        _agent = this.GetComponent<NavMeshAgent>();
        StartCoroutine(ChangeDestination());
    }

    IEnumerator ChangeDestination()
    {
        while(true)
        {
            _agent.SetDestination(Random.insideUnitSphere* PeapleInside.Instance.FakeFloor.localScale.x*5 + PeapleInside.Instance.FakeFloor.position );
            
            yield return new WaitForSeconds(Random.Range(_timeRangeOfRec.x, _timeRangeOfRec.y));
        }
    }
}
