﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;

    [RequireComponent(typeof(AudioSource))]
    public class SoundManager : MonoBehaviour
    {
        private static SoundManager _instance;
        [SerializeField] private GameObject _audioEmitterPrefab;
        [SerializeField] [Range(0f, 1f)] private float _globalVolume = 1f;
    [SerializeField] [Range(0f, 1f)] private float _musicVolume = 1f;
    [SerializeField] [Range(0f, 1f)] private float _soundVolume = 1f;
    [System.Serializable]
        public class Sound
        {
            [Tooltip("key to activate sound ")]
            public string key;//key to activate sound
            [Tooltip("max volume of sound")]
            [Range(0f, 1f)] public float soundVolume = 1;//max volume of sound
            [Tooltip("clips - from them one is randomly selected to be played")]
            public AudioClip[] clips;//clips - from them one is randomly selected to be played
            [HideInInspector]
            public int simultaneousPlayCount = 0;//track of number actual playing sounds of this object
        }

        [Header("Max number allowed of same sounds playing together")]
        public int maxSimultaneousSounds = 7;
        [Tooltip("Update this list in runtime dont work. Script on start create a dictionary from this list. Keays are values names key in Sound object ")]
        public List<Sound> _sounds = new List<Sound>();
        private Dictionary<string, Sound> _soundDictionary;

        private AudioSource _musicSource;
        private const string MUTE_PREF_KEY = "MutePreference";
        private const int MUTED = 1;
        private const int UN_MUTED = 0;
        private const string MUSIC_PREF_KEY = "MusicPreference";
        private const int MUSIC_OFF = 0;
        private const int MUSIC_ON = 1;

        public static SoundManager Instance { get => _instance; }
    public float MusicVolume { get => _musicVolume; set { _musicVolume = Mathf.Clamp01(value); _musicSource.volume = _soundDictionary["background"].soundVolume * _globalVolume * _musicVolume; } }
    public float SoundVolume { get => _soundVolume; set { _soundVolume = Mathf.Clamp01(value);  } }
    public float GlobalVolume { get => _globalVolume; set { _globalVolume = Mathf.Clamp01(value); _musicSource.volume = _soundDictionary["background"].soundVolume * _globalVolume * _musicVolume; } }

    void Awake()
        {
            if (Instance!=null)
            {
                DestroyImmediate(this.gameObject);
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            _musicSource = this.GetComponent<AudioSource>();
            _soundDictionary = new Dictionary<string, Sound>();
            for (int i = 0; i < _sounds.Count; i++)
            {
                _soundDictionary.Add(_sounds[i].key, _sounds[i]);
            }
        }

    }
        
        /// <summary>
        /// Plays the given sound with option to progressively scale down volume of multiple copies of same sound playing at
        /// the same time to eliminate the issue that sound amplitude adds up and becomes too loud.
        /// </summary>
        /// <param name="sound">Sound.</param>
        /// <param name="autoScaleVolume">If set to <c>true</c> auto scale down volume of same sounds played together.</param>
        /// <param name="maxVolumeScale">Max volume scale before scaling down.</param>
        public void PlaySound(string key, bool autoScaleVolume = true, float maxVolumeScale = 1f)
        {
            { StartCoroutine(CRPlaySound(_soundDictionary[key],this.transform.position, autoScaleVolume, maxVolumeScale)); }

        }
    public void PlaySound(string key,Vector3 pos, bool autoScaleVolume = true, float maxVolumeScale = 1f)
    {
        { StartCoroutine(CRPlaySound(_soundDictionary[key], pos, autoScaleVolume, maxVolumeScale)); }

    }

    IEnumerator CRPlaySound(Sound sound,Vector3 pos, bool autoScaleVolume = true, float maxVolumeScale = 1f)
        {
            if (sound.simultaneousPlayCount >= maxSimultaneousSounds)
            {
                yield break;
            }

            sound.simultaneousPlayCount++;

            float vol = maxVolumeScale* sound.soundVolume* _globalVolume*_soundVolume;

            // Scale down volume of same sound played subsequently
            if (autoScaleVolume && sound.simultaneousPlayCount > 0)
            {
                vol /= (float)(sound.simultaneousPlayCount);
            }
            AudioSource audioSorce =LeanPool.Spawn(_audioEmitterPrefab, this.transform).GetComponent<AudioSource>();
            audioSorce.transform.position = pos;
            audioSorce.mute = IsMuted();
            audioSorce.PlayOneShot(sound.clips[Random.Range(0, sound.clips.Length)], vol);
            LeanPool.Despawn(audioSorce.gameObject, sound.clips[0].length);
            
            // Wait til the sound almost finishes playing then reduce play count
            float delay = sound.clips[0].length * 0.7f;

            yield return new WaitForSeconds(delay);

            sound.simultaneousPlayCount--;

        }
        public AudioSource PlayMusic(string key="background",bool loop=true)
        {
            _musicSource.clip = _soundDictionary[key].clips[Random.Range(0, _soundDictionary[key].clips.Length)];
            _musicSource.loop = loop;
        _musicSource.volume = _soundDictionary[key].soundVolume * _globalVolume * _musicVolume;
            _musicSource.Play();
            if(IsMuted() || IsMusicOff())
            {
                _musicSource.mute = true;
            }
        return _musicSource;
        }
        /// <summary>
        /// Plays the given music.
        /// </summary>
        /// <param name="music">Music.</param>
        /// <param name="loop">If set to <c>true</c> loop.</param>
       
        public bool IsMuted()
        {
            return (PlayerPrefs.GetInt(MUTE_PREF_KEY, UN_MUTED) == MUTED);
        }

        public bool IsMusicOff()
        {
            return (PlayerPrefs.GetInt(MUSIC_PREF_KEY, MUSIC_ON) == MUSIC_OFF);
        }

        /// <summary>
        /// Toggles the mute status.
        /// </summary>
        public void ToggleMute()
        {
            // Toggle current mute status
            
            if (!IsMuted())
            {
                // Muted
                PlayerPrefs.SetInt(MUTE_PREF_KEY, MUTED);
                _musicSource.mute = true;
            }
            else
            {
                // Un-muted
                PlayerPrefs.SetInt(MUTE_PREF_KEY, UN_MUTED);
                _musicSource.mute = IsMusicOff();

            }
        }


        /// <summary>
        /// Toggles the mute status.
        /// </summary>
        public void ToggleMusic()
        {

            if (IsMusicOff())
            {
                PlayerPrefs.SetInt(MUSIC_PREF_KEY, MUSIC_ON);
                _musicSource.mute = IsMuted();
            }
            else
            {
                PlayerPrefs.SetInt(MUSIC_PREF_KEY, MUSIC_OFF);
                _musicSource.mute = true;
            }
        }


    }

