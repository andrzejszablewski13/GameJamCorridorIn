using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EndType
{
    win,lost
}
public class EndDet : MonoBehaviour
{
    // Start is called before the first frame update
    
    public delegate void Ended(EndType end);
    public static Ended EndReached;
    [System.Serializable]
    public struct OneEndSettings
    {
        public MainTypes observedType;
        public float valueReq;
        public bool above;
        public EndType end;
    }
    [SerializeField] private OneEndSettings[] _endSettings;
    private void OnDisable()
    {
        EndReached = null;
    }
    void Start()
    {
        GameMenager.OnAngerChange += ObserveValueAnger;
        GameMenager.OnMoneyChange += ObserveValueMoney;
        GameMenager.OnPeapleChange += ObserveValuePeaple;
    }
    private void ObserveValueMoney(float value)
    {
        ObserveValue(value, MainTypes.Money);
    }
    private void ObserveValuePeaple(float value)
    {
        ObserveValue(value, MainTypes.Peaple);
    }
    private void ObserveValueAnger(float value)
    {
        ObserveValue(value, MainTypes.Anger);
    }
    private void ObserveValue(float value,MainTypes type)
    {
        foreach (OneEndSettings item in _endSettings)
        {
            if(item.observedType==type && CheckIfValueOverpass(value,item.valueReq,item.above))
            {
                CanvasMenager.Instance.EndCanvas(item.end);
                EndReached?.Invoke(item.end);
            }
                
        }
    }
    private bool CheckIfValueOverpass(float value,float border,bool above)
    {
        if(above)
        {
            return value >= border;
        }
        return value <= border;
    }
}
