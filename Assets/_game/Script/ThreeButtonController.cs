using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ThreeButtonController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float _timeOut=1f;
    [System.Serializable]
    public struct Objects
    {
        public GameObject[] objectArray;
    }
    private double _lastTimeActivation=0;
    [System.Serializable]
    public struct ButtonSettings
    {
        public MainTypes _type1;
        [Tooltip("x is min y is max to random range")]public Vector2 _value1;
        public MainTypes _type2;
        [Tooltip("x is min y is max to random range")] public Vector2 _value2;

       [Tooltip("if wischto affect any y parametr")] public bool affectByValue;
       [Tooltip("by what parametr")] public MainTypes _affectType;
       [Tooltip("how much affect value of type 1, if 0 then no affect")] public float _factor1;
       [Tooltip("how much affect value of type 2, if 0 then no affect")] public float _factor2;
        
    }
    [Header("ButtonSettings")]
    [SerializeField] private ButtonSettings _buttonCallDownSet;
    [SerializeField] private ButtonSettings _buttonchickenSet;
    [SerializeField] private ButtonSettings _buttonbardSet;
    [Header("Characters")]
    [SerializeField] private Animator _barman;
    [SerializeField] private Animator _bard;
    [Header("Objects on state")]
    [SerializeField] private Objects _obCallDown,_obBard,_obChicken;
    void Awake()
    {
    }
    private void Start()
    {
        CanvasMenager.Instance.bard.onClick.RemoveAllListeners();
        CanvasMenager.Instance.bard.onClick.AddListener(() => { TurnChangeValues(_buttonbardSet); });
        CanvasMenager.Instance.calmDown.onClick.RemoveAllListeners();
        CanvasMenager.Instance.calmDown.onClick.AddListener(() => { TurnChangeValues(_buttonCallDownSet); });
        CanvasMenager.Instance.chicken.onClick.RemoveAllListeners();
        CanvasMenager.Instance.chicken.onClick.AddListener(() => { TurnChangeValues(_buttonchickenSet); });
    }
    private void TurnChangeValues(ButtonSettings settings)
    {
        if (Time.time >= _lastTimeActivation + _timeOut)
        {
            _lastTimeActivation = Time.time;
            float t = 1;
            foreach (GameObject item in _obCallDown.objectArray)
            {
                item.SetActive(false);
            }
            foreach (GameObject item in _obBard.objectArray)
            {
                item.SetActive(false);
            }
            foreach (GameObject item in _obChicken.objectArray)
            {
                item.SetActive(false);
            }
            switch (settings._affectType)
            {
                case MainTypes.Money:
                    t = GameMenager.Instance.Money;
                    _barman.SetTrigger("calmDown");
                    SoundManager.Instance.PlaySound("hittable");
                    foreach (GameObject item in _obCallDown.objectArray)
                    {
                        item.SetActive(true);
                    }
                    break;
                case MainTypes.Peaple:
                    t = GameMenager.Instance.Peaple;
                    _barman.SetTrigger("serveBeer");
                    foreach (GameObject item in _obChicken.objectArray)
                    {
                        item.SetActive(true);
                    }
                    if (t<8)
                    {
                        SoundManager.Instance.PlaySound("cheerlow");
                    }else
                    {
                        SoundManager.Instance.PlaySound(t<16? "cheermed" : "cheerhigh");
                    }
                    break;
                case MainTypes.Anger:
                    foreach (GameObject item in _obBard.objectArray)
                    {
                        item.SetActive(true);
                    }
                    t = GameMenager.Instance.Anger;
                    SoundManager.Instance.PlaySound("bard");
                    _bard.SetTrigger("BardPlay");
                    break;
                default:
                    break;
            }

            switch (settings._type1)
            {
                case MainTypes.Money:
                    GameMenager.Instance.Money += GetAddValue(settings._value1, settings.affectByValue, t, settings._factor1);
                    break;
                case MainTypes.Peaple:
                    GameMenager.Instance.Peaple += GetAddValue(settings._value1, settings.affectByValue, t, settings._factor1);
                    break;
                case MainTypes.Anger:
                    GameMenager.Instance.Anger += GetAddValue(settings._value1, settings.affectByValue, t, settings._factor1);
                    break;
                default:
                    break;
            }
            switch (settings._type2)
            {
                case MainTypes.Money:
                    GameMenager.Instance.Money += GetAddValue(settings._value2, settings.affectByValue, t, settings._factor2);
                    break;
                case MainTypes.Peaple:
                    GameMenager.Instance.Peaple += GetAddValue(settings._value2, settings.affectByValue, t, settings._factor2);
                    break;
                case MainTypes.Anger:
                    GameMenager.Instance.Anger += GetAddValue(settings._value2, settings.affectByValue, t, settings._factor2);
                    break;
                default:
                    break;
            }
            TimeMenager.Instance.NextTurn();
        }else
        {
            SoundManager.Instance.PlaySound("buttonfail");
        }
    }
    private float GetAddValue(Vector2 range,bool affect, float t,float factor)
    {
        return Random.Range(range.x, range.y) * (affect && factor!=0 && t!=0 ?Mathf.Log( t*factor,2) : 1);
    }
}
