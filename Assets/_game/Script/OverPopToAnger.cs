using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverPopToAnger : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float _borderPopulation;
    [SerializeField] private float _popAngerRatio = 1;
    void Start()
    {
        GameMenager.OnPeapleChange += (float value) =>
          {
              if(value>_borderPopulation)
              {
                  GameMenager.Instance.Anger += (value - _borderPopulation) * _popAngerRatio;
                  GameMenager.Instance.Peaple = _borderPopulation;
              }
          };
    }

}
