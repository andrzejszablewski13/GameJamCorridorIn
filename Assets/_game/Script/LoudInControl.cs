using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoudInControl : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource _audio;
    [SerializeField] private float _maxVolume = 0.5f,_maxPiple=25,_minPipToStart=5;
    private void Awake()
    {
        _audio = this.GetComponent<AudioSource>();
    }
    void Start()
    {
        GameMenager.OnPeapleChange += SetVolume;
    }
    private void SetVolume(float value)
    {
        if (value >= _minPipToStart)
        {
            _audio.volume = _maxVolume * Mathf.Clamp(value, 0, _maxPiple) / _maxPiple;

        }else
        {
            _audio.volume = 0;
        }
    }
}
