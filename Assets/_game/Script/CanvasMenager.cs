using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class CanvasMenager : MonoBehaviour
{
    // Start is called before the first frame update
    private static CanvasMenager _instance;

    public static CanvasMenager Instance { get => _instance;}
    public Image _angerText, _peapleText, _moneyText;
    [SerializeField] private float _maxAnger=100, _maxPeaple=25, _maxMoney=100;
    public Button calmDown, chicken, bard;
    public TextMeshProUGUI time, day;
    public GameObject lost, win,start;
    public Button startButton;
    public Button restartLostBut, restartWinBut;
    void Awake()
    {
        _instance = this;
        lost.SetActive(false); win.SetActive(false);
        GameMenager.OnAngerChange += (float value) => { _angerText.fillAmount =Mathf.Clamp01(value / _maxAnger); };
        GameMenager.OnMoneyChange += (float value) => { _moneyText.fillAmount = Mathf.Clamp01(value / _maxMoney); };
        GameMenager.OnPeapleChange += (float value) => { _peapleText.fillAmount = Mathf.Clamp01(value / _maxPeaple); };
        TimeMenager.OnNextDay += (int value) => { day.text = "Day: " + Mathf.FloorToInt(value); };
        TimeMenager.OnNewTime += (float value) => { time.text = "Time: " + (Mathf.FloorToInt(value) / 60f).ToString("n2"); };
        
        startButton.onClick.RemoveAllListeners();

        restartLostBut.onClick.RemoveAllListeners();
        restartWinBut.onClick.RemoveAllListeners();

    }
    
    private void Start()
    {
        // EndDet.EndReached += EndCanvas;
        startButton.onClick.AddListener(() => { start.SetActive(false); SoundManager.Instance.PlaySound("click"); });
        restartLostBut.onClick.AddListener(() => { GameMenager.Instance.ReloadGame(); SoundManager.Instance.PlaySound("click"); });
        restartWinBut.onClick.AddListener(() => { GameMenager.Instance.ReloadGame(); SoundManager.Instance.PlaySound("click"); });
    }
    public void EndCanvas(EndType end)
    {
        if (!win.activeSelf && !lost.activeSelf)
        {
            switch (end)
            {
                case EndType.win:
                    win.SetActive(true);
                    break;
                case EndType.lost:
                    lost.SetActive(true);
                    break;
                default:
                    break;
            }
        }
    }

}
