using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeMenager : MonoBehaviour
{
    private static TimeMenager _instance;
    public delegate void NextDayHappend(int value);
    public static NextDayHappend OnNextDay;
    public delegate void NewTime(float timeInMin);
    public static NewTime OnNewTime;
    public static TimeMenager Instance { get => _instance; }
    public float TimeOfDayInMin { get => _timeOfDayInMin;set{ _timeOfDayInMin = value;OnNewTime?.Invoke(_timeOfDayInMin); } }
    public int DayNumber { get => _dayNumber;set { _dayNumber = value;OnNextDay?.Invoke(_dayNumber); } }

    [SerializeField] private int _turnsPerDay;
    [SerializeField] private float _startHour=8;
    [SerializeField] private float _endHour=20;
    private float _minutesOfWorkPerDay;
    private float _minutePerTurn;
    private float _timeOfDayInMin;
    private int _dayNumber = 0;
    // Start is called before the first frame update
    private void OnDisable()
    {
        OnNextDay = null;
        OnNewTime = null;
    }
    private void Awake()
    {
        _instance = this;
    }
    void Start()
    {
        NextDay();
        _minutesOfWorkPerDay = _endHour * 60 - TimeOfDayInMin;
        _minutePerTurn=_minutesOfWorkPerDay/(_turnsPerDay);
    }
    public float NextTurn()
    {
        TimeOfDayInMin += _minutePerTurn;
        if(TimeOfDayInMin + _minutePerTurn/2>=_endHour*60)
        {
            NextDay();
        }
        return TimeOfDayInMin;
    }
    public int NextDay()
    {
        TimeOfDayInMin = _startHour * 60;
        DayNumber++;
        return DayNumber;
    }

}
