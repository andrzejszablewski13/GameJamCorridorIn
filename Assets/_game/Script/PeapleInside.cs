using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
public class PeapleInside : MonoBehaviour
{
    [SerializeField] private Sprite[] _peples;
    [SerializeField] private GameObject _preFab;
    [SerializeField] private Vector3 _startPos;
    [SerializeField] private float _personEveryPep=3.2f;
    private Queue<GameObject> _spanwedPeaple;
    public Transform FakeFloor;
    private static PeapleInside _instance;

    public static PeapleInside Instance { get => _instance; }

    // Start is called before the first frame update

    private void Awake()
    {
        _instance = this;
        _spanwedPeaple = new Queue<GameObject>();
    }
    private void Start()
    {
        GameMenager.OnPeapleChange += ControlPeapleNumber;
        ControlPeapleNumber(GameMenager.Instance.Peaple);
    }
    private void ControlPeapleNumber(float value)
    {
        if(Mathf.Abs(_personEveryPep*_spanwedPeaple.Count-GameMenager.Instance.Peaple)>=_personEveryPep)
        {
            float t = (_personEveryPep * _spanwedPeaple.Count - GameMenager.Instance.Peaple )/ _personEveryPep;
            if(t<0)
            {
                for (int i = 0; i < Mathf.CeilToInt(-t); i++)
                {
                    SpawnPerson();
                }
            }else
            {
                for (int i = 0; i < Mathf.CeilToInt(t); i++)
                {
                    DeSpawPerson();
                }
            }
        }
    }
    public void SpawnPerson()
    {
        GameObject go = Instantiate(_preFab, this.transform);
        go.transform.position = _startPos;
        go.GetComponentsInChildren<SpriteRenderer>()[1].sprite = _peples[Random.Range(0, _peples.Length)];
        _spanwedPeaple.Enqueue(go);
    }
    public void DeSpawPerson()
    {
        GameObject go = _spanwedPeaple.Dequeue();
        go.GetComponentInChildren<AiMoving>().StopAllCoroutines();
        go.GetComponentInChildren<NavMeshAgent>().SetDestination(_startPos);
        go.GetComponentsInChildren<SpriteRenderer>()[1].DOFade(0.1f, 5);
        Destroy(go, 5);
    }
}
